# Wordgame Solver

## About

Small script written in python to find a list of possible words from a seqence of letters. The list of possible words is defined in the file _words_ and will be loaded when running the script.

## Usage

Run the scipt with python3:

    python3 wordgame_solver.py

The script will execute an input loop. You have to input two parameters:
- a sequence of letters, you want to find possible words for
- the desired length of the words you like to find. (must be at least the length of the entered sequence, but can also be lower)

The script will automatically determine the entered parameter, depending if you entered a valid number or a sequence of letters.

After entering the data the script will output a list of all possible words, containing letters of the entered sequence, having the desired length.
