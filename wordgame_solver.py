#!/usr/bin/env python3

# ------------------------------------------------------------------------------
def buildPermutations(letters, wordLength, word = "", permutations = None):
    if permutations is None:
        permutations = set()

    if wordLength > 0 and len(letters) > 0:
        for i in range(0, len(letters)):
            currentLetter = letters[i]

            lettersCopy = letters.copy()
            lettersCopy.remove(currentLetter)

            buildPermutations(lettersCopy, wordLength - 1, word + currentLetter, permutations)
    else:
        permutations.add(word)

    return list(permutations)

# ------------------------------------------------------------------------------
def buildDict(filePath):
    print(filePath)
    file = open(filePath, encoding = "iso-8859-1")

    dict = set()
    for line in file:
        word = line.strip()
        word = word.lower()
        dict.add(word)

    return dict

# ------------------------------------------------------------------------------
def getHits(permutations, dict):
    hits = []
    for perm in permutations:
        if perm in dict:
            hits.append(perm)
    return hits

# ------------------------------------------------------------------------------
def runInputLoop(dict):
    letters = ""
    lengths = [0]
    lenghtPrefix = None

    while True:
        print("###############################################################")

        inp = input(">>> enter letters or word-length: ")

        if inp.startswith('+'):
            lenghtPrefix = '+'
        elif inp.startswith('-'):
            lenghtPrefix = '-'
        else:
            lenghtPrefix = None

        try:
            length = int(inp.strip())

            if lenghtPrefix == '+':
                lengths.append(length)
            elif lenghtPrefix == '-':
                lengths.remove(abs(length))
            else:
                lengths = [length]
        except:
            letters = list(inp.lower())

        permutations = list()
        for length in lengths:
            permutations.extend(buildPermutations(letters, length))
#        print("permutations: " + str(permutations))

        hits = getHits(permutations, dict)

        print("CURRENT LETTERS: " + str(letters))
        print("CURRENT WORD LENGTH: " + str(lengths))
        print("POSSIBLE WORDS: " + str(hits))

# --------------------------------- main ---------------------------------------

print("Initializing word list ...")
dict = buildDict("words")
runInputLoop(dict)
